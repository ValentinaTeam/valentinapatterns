# README #

## License ##

**CC0 1.0 Universal (CC0 1.0)** 
Public Domain Dedication  
This is a human-readable summary of the Legal Code (read the full text).  

**Disclaimer**  
No Copyright  
 
This license is acceptable for Free Cultural Works.  
The person who associated a work with this deed has dedicated the work to the public domain by waiving all of his or her rights to the work worldwide under copyright law, including all related and neighboring rights, to the extent allowed by law.  

You can copy, modify, distribute and perform the work, even for commercial purposes, all without asking permission. See Other Information below.  

**Other Information**  
In no way are the patent or trademark rights of any person affected by CC0, nor are the rights that other persons may have in the work or in how the work is used, such as publicity or privacy rights.  
Unless expressly stated otherwise, the person who associated a work with this deed makes no warranties about the work, and disclaims liability for all uses of the work, to the fullest extent permitted by applicable law.  

When using or citing the work, you should not imply endorsement by the author or the affirmer.  
  
# Valentina Patterns #

### About this repository ###

* ValentinaPatterns contains design and measurement files to use with Valentina pattern design software, downloadable from https://valentina-project.org.

### Current version ###

* v0.0.1

### How do I get set up? ###

* [Download Valentina](https://valentina-project.org)
* [Learn Valentina](https://www.youtube.com/channel/UC61WgbNdZ_wqxKf9PzJrjgA)
* Install **zip** and **unzip** utilities, available elsewhere for Linux, Mac, and Windows
* Deployment instructions:
* Download ValentinaPattern .zip files to a temp directory
* Unzip each .zip file
* Copy the design files to your Valentina pattern directory
* Copy the .png files to your Valentina pattern directory
* Copy the measurement files to your Valentina measurement directory

### Contribution guidelines ###

* Anyone can post comments and problems in the issues list
* Anyone can modify the downloaded files and push the modified file

### Who do I talk to? ###

* Susan Spencer, susan@valentina-project.org